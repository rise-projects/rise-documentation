.. RISE documentation master file, created by
   sphinx-quickstart on Mon Apr 17 08:35:44 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to the Documentation of RISE! 
=====================================

.. image:: images/RISE_Logo.png
   :align: center
   :scale: 20%
   :class: logo

The open-source architecture **RISE** (A Robotics Integration and Scenario-Management Extensible-Architecture) is a tool for researcher that want to create Human-Robot-Interaction (HRI) scenarios.
RISE offers the possibility to connect initially with the robots Nao, Pepper and is extensible for the use of own robots with custom actions (e.g. the agent Floka)!

The software is developed within the Transregional Collaborative Research Centre "Constructing Explainability" (`TRR 318 <https://trr318.uni-paderborn.de/en/>`_) and is used for internal projects as well as in coorperation with partners.
The main focus of the system is to support the work in interdisciplinary teams and to enable also non-computer scientists to create, discuss and understand behaviors of robots in HRI.

.. raw:: html

   <div style="width: 100%; overflow: hidden;">
      <img src="_static/rise_control_via_ir.gif" alt="RISE with Floka GUI overview" style="width: 100%; max-width: 100%; height: auto;">
   </div>
|
**Hint:** This documentation and also the source-code (`Gitlab: RISE-Project <https://gitlab.ub.uni-bielefeld.de/rise-projects>`_) of this project is under development mainly by (PhD) students and research assistants. 
The system and its documentation exhibit a continuous state of development and change.

   .. toctree::
      :hidden:
      
      gettingStarted
      user-interface
      structures
      architecture
      tutorialOverview
      api
      glossary
      contact


.. Indices and tables
   ==================

   * :ref:`genindex`
   * :ref:`modindex`
   * :ref:`search`