API
=============

RISE is designed to allow external components to start and interrupt configurable Behaviors on the RISE-Scheduler. These Behaviors can be configured internally and are controllable from other applications. The way of controlling such behaviors is **ROS**.

## Using RISE-Functions

### Use RISE-Functions inside a DomainTask

```bash
  Topic: /rise/set/domainTask/goal
  Message: ros_tcp_endpoint/DomainTaskActionGoal

  rostopic pub -1 /rise/set/domainTask/goal ros_tcp_endpoint/DomainTaskActionGoal
  "header:
    seq: 0
    stamp:
        secs: 0
        nsecs: 0
    frame_id: ''
    goal_id:
    stamp:
        secs: 0
        nsecs: 0
    id: ''
    goal:
    domain_task_behavior: ''
    function_type: ''
    calling_type: ''" 
```

| Parameter              | Type     | Description                                                      |
|:-----------------------|:---------|:-----------------------------------------------------------------|
| `domain_task_behavior` | `string` | **Required**. Name or description of the Behavior                |
| `function_type`        | `string` | **Required**. Name of Function [Function-Types](#function-types) |
| `calling_type`         | `string` | `None` or `Blocking` [Calling-Types](#calling-types)             |


(function-types)=
### Function-Types and Behaviors

| Function-Type                | Description              | Example DomainTask-Behaviors                    |
|:-----------------------------|:-------------------------|:------------------------------------------------|
| `startCommunicationRobotAct` | Start CRA on scheduler   | Name of CRA which is defined in Configurations  |
| `stopCommunicationRobotAct`  | Stop CRA                 | Name of CRA which is defined in Configurations  |
| `startInteractionRule`       | Start IR                 | Name of IR which is defined in Configurations   |
| `stopInteractionRule`        | Stop IR                  | Name of IR which is defined in Configurations   |
| `raiseEventTopic`            | Raise internal EvenTopic | Name of Topic which should be raised internally |
| `assignValue`                | Set Working-Memory entry | Json Key-Value Pair: `{"key" : value}`          |


(calling-types)=
### Calling-Types

* `None`: Goal is handled without any restrictions for scheduling and is not blocking other tasks and can be interrupted.
* `Blocking`: Goal cannot be interrupted by other goals.


### Cancel a DomainTask

```bash
  Topic: /rise/set/domainTask/cancel
  Message: actionlib_msgs/GoalID

  rostopic pub -1 /rise/set/domainTask/cancel actionlib_msgs/GoalID 
  "stamp:
    secs: 0
    nsecs: 0
    id: ''" 
```

| Parameter | Type     | Description                              |
|:----------|:---------|:-----------------------------------------|
| `id`      | `string` | **Required**. Id of DomainTask to cancel |





## Request Working Memory Informations

```bash
  Topic: /rise/set/memoryService
  Call: /rise/set/memoryService "request_by_key: ''"

  rosservice call /rise/set/memoryService "request_by_key: ''" 
```

| Parameter               | Type     | Description                                           |
|:------------------------|:---------|:------------------------------------------------------|
| Input: `request_by_key` | `string` | **Required**. Key in Working-Memory                   |
| Output: `json_response` | `string` | **Required**. Json Key-Value Pair ("{\"key\":value}") |


## Receiving Informations about DomainTasks

### Receive Result of a DomainTask

```bash
  Topic: /rise/set/domainTask/result
  Message: ros_tcp_endpoint/DomainTaskActionResult
```

| Parameter    | Type      | Description                                   |
|:-------------|:----------|:----------------------------------------------|
| `task_id`    | `integer` | **Required**. Id of DomainTask                |
| `result`     | `integer` | **Required**. Result in 1 (true) or 0 (false) |

### Receive Status of a DomainTask

```bash
  Topic: /rise/set/domainTask/status
  Message: actionlib_msgs/GoalStatusArray

  rostopic echo /rise/set/domainTask/status
```

### Receive Feedback of a DomainTask

```bash
  Topic: /rise/set/domainTask/feedback
  Message: ros_tcp_endpoint/DomainTaskActionFeedback

  rostopic echo /rise/set/domainTask/feedback
```

## Receive Status of internal RISE-States

```bash
  Topic: /rise/state/update
  Message: std_msgs/String
```

| Parameter | Type     | Description                         |
|:----------|:---------|:------------------------------------|
| `data`    | `string` | **Required**. Current State of RISE |

Example of the output of a message from RISE to external components about the previos state (`WaitingForTimer`), the current state (`AutonomousLife`) and the state-change as transition (`/R_Wait_Idle_end`) of a specific InteractionRule (`R_background_behavior`). 

```bash
data: "{\"interactionrule_name\":\"R_background_behavior\",
        \"previous_state\":\"WaitingForTimer\",
        \"transition_event\":\"/R_Wait_Idle_end\",
        \"current_state\":\"AutonomousLife\"}"
```

You can access this update message in Python, for instance, by deserializing the values by keys from the JSON format:

```bash
rise_state_prev_state = json.loads(data.data)["previous_state"]
rise_state_event      = json.loads(data.data)["transition_event"]
```