# How to create a Human-Robot-Interaction Scenario

## Introduction

In this tutorial, I will explain how to create a Human-Robot Interaction (HRI) scenario using a sample scenario as an example. The sample scenario we will be working with involves Floka greeting the user and engaging in a short conversation. Throughout the tutorial, we will work with Communication Robot Acts (CRA), Interaction Rules (IR), and Working Memory which is required for creating this scenario using RISE.

## Requirements

- RISE installed on your computer.
- ROS installed on your linux machine.

## Step-by-step instructions

1. Step: Create a CRA.xml

    ```xml
    <Agent-Dialogs>

	    <Dialog name = "AskForName">
		    <Speech>
			    <actionID>1</actionID>
			    <message>Hello, I'm Floka, what's your name</message>
		    </Speech>
	    </Dialog>
    </Agent-Dialogs>
    ```
   
    This block of code lets Floka ask the name of the user. <br>


2. Step: Add another dialog to CRA.xml

    ```xml
    <Agent-Dialogs>

	    <Dialog name = "GreetPerson">
		    <Speech>
			    <actionID>1</actionID>
			    <message>Hi %Person1%, it's nice to meet you.</message>
		    </Speech>
	    </Dialog>
    </Agent-Dialogs>
    ```
   
    This lets Floka use the variable Person1 to greet the user. <br>


3. Step: The CRA.xml should now look like this:

    ```xml
    <Agent-Dialogs>

        <Dialog name = "AskForName">
            <Speech>
                <actionID>1</actionID>
                <message>Hello, I'm Floka, what's your name</message>
            </Speech>
        </Dialog>

        <Dialog name = "GreetPerson">
            <Speech>
                <actionID>1</actionID>
                <message>Hi %Person1%, it's nice to meet you.</message>
            </Speech>
        </Dialog>
    </Agent-Dialogs>
    ```


4. Step: Create an IR.xml

    ```xml
    <InteractionRules>

        <InteractionRule name = "InteractionRule" initial = "State 1" priority = "1">

            <State id = "State 1">
                <onEntry>
                    <function startCommunicationRobotAct = "AskForName" />
                </onEntry>

                <transition eventTopic = "/AskForName_end" target = "State 2"/>
            </State>

            <State id = "State 2">
                <onEntry>
                    <function startCommunicationRobotAct = "GreetPerson" />
                </onEntry>
            </State>

        </InteractionRule>

    </InteractionRules>
    ```

    This xml file contains the Interaction Rule: InteractionRule which will begin with State 1 which will then start the CRA: AskForName. After that State 2 will be started.


5. Step: Create a working memory 

    ```json
    {"WorkingMemory":
      [
         {"Person1" : "Peter"}
      ]
    }
    ```
    This json file contains the variable Person1 which has the string value of "Peter". <br>


6. Step: Simulate speech recognition with ROS input (optional)

   
    To modify working memory variables, you can use ROS to publish a rostopic by using the following parameterization: FunctionType: assignValue, domain_task_behavior: {"key": value}.  <br>


7. Step: Start RISE 

    You should see something like this:

    |      <img alt="Picture of rise_standard_ui" src="../_static/images/rise_standard_ui.png" width="100%" />       | 
    |:-------------------------------------------------------------------------:|
    | *RISE UI with no active CRA, IR, working Memory and no valid ip-address.* |

	Now we have to enter the right ip-address and the root directory path in the settings and hit refresh. You will now be able to select a category and project from the drop-down menu.

    |  <img alt="Picture of rise_standard_ui2" src="../_static/images/rise_standard_ui2.png" width="100%" />   |
    |:-------------------------------------------------------------------:|
    | *RISE UI with active CRA, IR, working Memory and valid ip-address.* |

	You should now see the Communication Robot Acts, Interaction Rules and Working Memory in the UI. If Floka does not speak, change from debug mode to Real.


