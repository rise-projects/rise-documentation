# How to use RISE as Wizard of Oz Tool?

Due to the easy usable User-Interface of the RISE-Core application, it is possible to develop a user study without implementing real nodes that fills data into the memory or even without creating complex Interaction Rules **(IR)**. 

Following two examples will be explained. One fully manual controlled example, without the use of the IRs and one semi autonomous, where the Wizard of Oz part is used to fill the memory. 

## Fully manuals controlled
One opportunity to simulate a reactive behavior of the robot is by creating for every needed reaction an own Communication Robot Act **(CRA)**. These CRAs could be a greeting at the beginning, a task explanation and some reactions like a smile and pre-defined answers. 

During the Study, the researcher need to be able to notice, what is happening during the study. The researcher than can use the predefined CRAs to react according to the study design.

## Pseudo autonomous  
Another possibility is, to design IRs with conditions. The idea is, to just modify the memory with e.g. a name and as soon as value is in the memory for the name, the next *State* of the IR is triggered. This is done by two alternating states until a value is set.

```xml
    <InteractionRules>
        <InteractionRule name = "GreedWithName" initial = "State 1" priority = "1">

		<State id = "State 1">
			<onEntry>
				<function startCommunicationRobotAct = "AskForName" />
			</onEntry>
			
			<transition eventTopic = "/AskForName_end" target = "State 2"/>
		</State>

		<State id = "State 2">
			<onEntry>
				<if cond = "Person1 != []">
					<function startCommunicationRobotAct = "GreetPerson" />
				<else>
					<function raiseEventTopic = "/No_Interaction"/>
				</else>
				</if>
			</onEntry>
			
			<transition eventTopic = "/No_Interaction" target = "State 3"/>
			<transition eventTopic = "/GreetPerson_end" target = "State 4"/>
		</State>

		<State id = "State 3">
			<onEntry>
				<function raiseEventTopic = "/stillWaiting"/>
			</onEntry>
			
			<transition eventTopic = "/stillWaiting" target = "State 2"/>
		</State>
		
		<State id = "State 4">
			<onEntry>
				<function startCommunicationRobotAct = "End" />
			</onEntry>
		</State>
        </InteractionRule>
    </InteractionRules>
```

This IR will first ask the Participant for a name. Afterwards this IR will constantly switch between *State 2* and *State 3*, waiting in *State 2* that a name is set in the Working Memory **(WM)**. 
The researcher can enter the name into the memory and the robot will greet the Participant with their name.
This is done throw the [RISE-Core UI](user-interface) in the WM Tab.

Certainly, this serves as a rudimentary example. It can be easily expanded by e.g. entering multiple *if cond* with defined value like *attention < 5*. It would also be possible to enter coordinates and manually trigger a CRA that adjust the gaze of the robot. There are plenty of possibilities without the need to develop nodes for Natural language processing, attention or other sensor inputs. 