# How integrate RISE to own Components?

To develop an autonomous HRI scenario, some sort of input or triggers are needed. In RISE this is done, by developing ROS Nodes, that send topics to trigger a transition of an Interaction Rule **(IR)** or fill the Working Memory **(WM)** with data. This data can be used by other nodes or directly in Communication Robot Acts **(CRAs)** or IRs.
The forthcoming section will present two brief examples.

## Writing with a node into the Memory and starting a CRA
Following a small example for a Feature Node is presented. This example generates a random number, that simulates the measurement of the current temperature. This temperature will be saved every second into *Temp* of the WM of the RISE-Core by sending a DomainTaskActionGoal message. A CRA then e.g. could let the Robot tell the current temperature when the Robot is asked to. 
Also in this example, when the random temperature is below 5 a Communication Robot Act with the name *"ItsGettingCold"* is started.

```python
#!/usr/bin/env python
import rospy
from std_msgs.msg import String
from rise_core.msg import DomainTaskActionGoal
import random
def tempPublisher():
    pub = rospy.Publisher('/rise/set/domainTask/goal', DomainTaskActionGoal, queue_size=10)
    rospy.init_node('TemperaturFeatureNode', anonymous=True)
    rate = rospy.Rate(1) 
    while not rospy.is_shutdown():
        currentTemp = random.randint(0,30)

        assignValueMsg = DomainTaskActionGoal()
        assignValueMsg.goal.function_type="assignValue"
        assignValueMsg.goal.domain_task_behavior=json.dumps({"Temp": str(currentTemp)})

        rospy.loginfo("assign Value: "+str(currentTemp))
        pub.publish(assignValueMsg)
        if(currentTemp<5):

            startCRAmsg = DomainTaskActionGoal()
            startCRAmsg.goal.function_type="startCommunicationRobotAct"
            startCRAmsg.goal.domain_task_behavior='ItsGettingCold'

            rospy.loginfo("start, ist getting cold CRA")
            pub.publish(startCRAmsg)
        rate.sleep()
if __name__ == '__main__':
    try:
        tempPublisher()
    except rospy.ROSInterruptException:
        pass
```

There are two possibilities to run this example, dependent on how RISE was installed. This is important, to be able to import the *DomainTaskActionGoal* message type from the ros_tcp_connector:

1. When the RISE is [build via Jenkins](../gettingStarted/installJenkins.md), it is possible to press in the left lower corner on the name of the machine, in this case *Precision*. 
In this terminal, the environment is loaded and the ros message DomainTaskActionGoal known. Therefore, it is possible to run the example script in this terminal.

|             ![platzhalter](../images/vdemo_open_terminal.svg)              |
|:--------------------------------------------------------------------------:|
| *Vdemo_Controller, how to open up a Terminal with the environment loaded.* |

2. If RISE is build [per manual build](../gettingStarted/installDev.md) it is necessary to run the script in a terminal after sourcing the setup.bash of the catkin workspace, the tcp_connector is build in. 
```bash
  source ~/catkin_ws/devel/setup.bash
  python3 $PathToTheScript/example.py
```

This is just a small example to build up more complex feature nodes with e.g. a webcams for with face recognition. The further possibilities like starting an IR or requesting a value from the memory are described in the [API](../api.md) section.


## Getting a value by key from the Working Memory

For accessing the Working Memory in a python script (that is run in Vdemo like in the example above) 
the following imports are needed.
```python
import rospy
from rise_core.srv import WorkingMemoryService
import json
```

This function accesses the Working Memory to get the pair responding to the key 
and then formats the json response so only the value is returned.
```python
def getValue(key):
    rospy.wait_for_service('/rise/set/memoryService')
    try:
        memoryService = rospy.ServiceProxy('/rise/set/memoryService', WorkingMemoryService)
        resp1 = memoryService(key)
        pair = json.loads(resp1.json_response)
        return pair[key]
    except rospy.ServiceException as e:
        print("Service call failed: %s"%e)
```
