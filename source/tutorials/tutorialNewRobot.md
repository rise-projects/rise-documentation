# How to integrate a new Robot

## 1. Adding a new Robot and Modalities to the *Robot-Enum*
In a first step you have to integrate your new Robot by name into the system. For this extend the RobotEnum.Robot class with your own Enum-Type of your new robot. Editing the file in ```Assets/Scripts/Structures/Enums/RobotEnum.cs```. In this example we integrate the new robot Temi into **RISE**.
   
```bash
public enum Robot
{
    Floka,
    Pepper,
    NAO,
    Temi
}
```

Create a new modality-list for your new Robot. It describes which actions your robot is able to perform:

```bash
private static List<ActionEnum.Action> modalities_temi = new List<ActionEnum.Action>()
{
    ActionEnum.Action.Speech,
    ActionEnum.Action.Emotion,
    ActionEnum.Action.Animation,
    ActionEnum.Action.LookAt
};
```

Add your ROS-Topic prefix into the function `GetTopicForRobot` for your new robot:

```bash
public static string GetTopicForRobot(Robot robot)
{
    switch (robot)
    {
        case Robot.Pepper:
            return "naoqi";

        case Robot.NAO:
            return "naoqi";

        case Robot.Floka:
            return "floka";

        case Robot.Temi:
            return "temi";

        default:
            break;
    }

    return null;
}
```

Afterwards append your robot in the switch-case instruction in the function ```GetPossibleActionListForRobot```.

```bash
public static List<ActionEnum.Action> GetPossibleActionListForRobot(Robot robot)
{
    switch (robot)
    {
        case Robot.Pepper:
            return modalities_pepper;

        case Robot.NAO:
            return modalities_nao;
        
        case Robot.Floka:
            return modalities_floka;

        case Robot.Temi:
            return modalities_temi;

        default:
            break;
    }

    return null;
}
```

## 2. Creating Actions for your Robot
Follow the instructions from [How To: Integrate custom BehaviorActions](#how-to-integrate-custom-behavioraction) for creating actions in **RISE** for your robot!
You can extend the system with your own actions needed for your robot or use the default actions.

## 3. Test your Implementation
After you have created all necessary components in **RISE** and you can test if the system sends ROS-Messages on your defined topics with your configured values, messages and parameters, the next step is to create a Robot-Wrapper and receive these information! For testing your implementations, check in a ROS-Terminal, if the output is as expected. **Keep in mind**: You will not receive any feedback actually from your robot, so the CRA will may not finish a single action!


## 4. Implement your own Robot-Wrapper
For implementing your own Robot-Wrapper, make sure **RISE** is sending on the right topic the expected message.
To develop your own wrapper follow the instructions:

1. **Create a ROS-Node with connection to your Robot's API**
   Implement your own node (in your preferred programming language) as catkin workspace ([Catkin-WS](http://wiki.ros.org/catkin/Tutorials/create_a_workspace)) and make sure you can access your robot via the API to your robot!
   <br>

2. **Import the ROS-Message Types in your node**
   Make sure you have access to your own created ROS-Message-Types. RISE is sending in the current version the actions via ROS in the format of HLRC-ROS-Messages from [HLRC-Messages](https://gitlab.ub.uni-bielefeld.de/rise-projects/application/tcp-connector/-/tree/include-hlrc-msgs/src/HLRC_SERVER/action).
   For integrating custom ROS-Messages in a catkin workspace see this [ROS-Tutorial for Custom Messages](http://wiki.ros.org/ROS/Tutorials/CreatingMsgAndSrv).
   <br>

3. **Subscribe Actions from RISE in your node**
   Add subscriber (or ROS-ActionServer) for your implemented action-types in your Robot-Wrapper [ROS Tutorial, Publisher and Subscriber](http://wiki.ros.org/ROS/Tutorials/WritingPublisherSubscriber%28c%2B%2B%29).
   <br>

4. **Process the received actions and call your Robot API**
   After receiving the ROS-Messages from **RISE**, process the actions and call the corresponding API-Functions from your own robot, to execute different behaviors.
   <br>

5. **Send Results of your Actions back to ROS**
   After the robot processes your actions, mainly the robot has finished an action - send a result as *ActionResult* back to **RISE** via ROS on the *Result-Topic*, if possible. If that is not possible, keep in mind to implement the result logic in the `BehaviorAction` in RISE.