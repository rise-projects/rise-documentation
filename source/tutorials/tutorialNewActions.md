How to customize RISE for own Use
=================================

## Overview

This page will provide a step-by-step guide for integrating custom actions and custom robots into **RISE**.
It will cover the process of creating and implementing custom actions, as well as configuring the system to work with custom robots.

## Requirements

- RISE installed on your computer.
- ROS installed on your (linux) machine.
- Modalities for your own Robot


## How To: Integrate Custom BehaviorAction

### 1. Identify necessary *Actions* needed for your Robot
**RISE** supports a default set of actions for simple robot behaviors like **Speech**, **LookAt**, **Emotion** and **Animation**.
In case of the need of additional custom actions for a robot that is already supported by the system, first define specifications of the action which is missing.
For the robots **NAO** and **Pepper** for example, we want to integrate the action **PointAt** into RISE, to allow the robots point at targets with their arm.


### 2. Adding Actions to the *Action-Enum*
In a first step you have to integrate your new Action by name into the system. For this expend the ActionEnum.Action class with your own Enum-Type of your new action. Editing the file in ```Assets/Scripts/Structures/Enums/ActionEnum.cs```
   
```bash
public enum Action
{
    None,
    Emotion,
    LookAt,
    Speech,
    Animation,
    PointAt
}
```

Additional, extend the functions ```GetBehaviorObjectByEnum``` for instantiating an empty object of your BehaviorAction which you will create in the next step and ```GetSpriteForAction``` for returning an Image/sprite for your action.
Finally, add the new action into the modality-lists for each robot in the file ```Assets/Scripts/Structures/Enums/RobotEnum.cs```


### 3. Create your own *BehaviorAction*
First, create you have to create your own **BehaviorAction** class to define your new action. For this, you have to implement the abstract class **BehaviorAction**. The functions ``Initialize, GetAttributeTags and StartBehaviorAction`` describes the way of initializing your action, defines the XML-tags for your actions and implements the execution of your action. Additionally, you have to define own attributes as properties of the action which describes the action itself. Add your own implementation of your action into ```Assets/Scripts/Structures/Actions/BehaviorActionPointAt.cs```.

**Example** for the **BehaviorAction** **PointAt**:

```bash
public class BehaviorActionPointAt : BehaviorAction
{
    public double x { get; set; }
    public double y { get; set; }
    public double z { get; set; }
    public float roll { get; set; }

    public override void Initialize(List<string> attributeList)
    {
        base.Initialize(attributeList);
        this.actionState = ActionEnum.Action.PointAt;

        if (IsAttributeValueVariable(attributeList[2]))
        {
            replaceableAttributes.Add("x", attributeList[2]);
        }
        else
        {
            x = double.Parse(attributeList[2]);
        }

        if (IsAttributeValueVariable(attributeList[3]))
        {
            replaceableAttributes.Add("y", attributeList[3]);
        }
        else
        {
            y = double.Parse(attributeList[3]);
        }

        if (IsAttributeValueVariable(attributeList[4]))
        {
            replaceableAttributes.Add("z", attributeList[4]);
        }
        else
        {
            z = double.Parse(attributeList[4]);
        }

        if (IsAttributeValueVariable(attributeList[5]))
        {
            replaceableAttributes.Add("roll", attributeList[5]);
        }
        else
        {
            roll = double.Parse(attributeList[5]);
        }
    }

    public override List<string> GetAttributeTags()
    {
        List<string> tagList = base.GetAttributeTags();
        tagList.Add("x");
        tagList.Add("y");
        tagList.Add("z");
        tagList.Add("roll");

        return tagList;
    }

    public override Dictionary<string, string> GetAttributeDescriptions()
    {
        Dictionary<string, string> attributes = new Dictionary<string, string>();

        attributes.Add("Point X", this.x.ToString());
        attributes.Add("Point Y", this.y.ToString());
        attributes.Add("Point Z", this.z.ToString());
        attributes.Add("Roll", this.roll.ToString());

        return attributes;
    }

    public override string DebugBehaviorAction()
    {
        return base.DebugBehaviorAction() + '\n' +
            "X: " + this.x + '\n' + 
            "Y: " + this.y + '\n' + 
            "Z: " + this.z + '\n' + 
            "Roll: " + this.roll;
    }

    public override IEnumerator StartBehaviorAction()
    {
        this.SetActionStatus(true);

        behaviorActionController.PublishTranslatedAction(this);
        yield return new WaitForSeconds(ActionEnum.APPROXIMATION_TIME);

        this.SetActionStatus(false);
    }
}
```

This example describes the action **PointAt** with 4 attributes with coordinate points and a rolling factor for the speed of a movement. The function StartBehaviorAction executed the action with the controller directly. Therefore, we have to define a waiting-condition for the action. For actions with unknown support for results of an *ActionGoal* you can use the ```ActionEnum.APPROXIMATION_TIME```. Otherwise, see **BehaviorActionSpeech** for an example with waiting for a *ActionGoalResult* (see [BehaviorActionSpeech-Code](https://gitlab.ub.uni-bielefeld.de/rise-projects/application/rise-core/-/blob/main/Assets/Scripts/Structures/Actions/BehaviorActionSpeech.cs)).


### 4. Extend the *BehaviorActionController*
1. **Implement custom ROS-Messages for your previously created BehaviorAction**: <br>
   In a first step you have to implement your own ROS-ActionClient for the newly created BehaviorAction.
   Therefore, create a ROS-Message file for your created action.
   Example of a message file for the action **PointAt** in ```PointAt.action``` file.
        
    ```bash
    geometry_msgs/PointStamped point

    # roll angle
    float32 roll

    ---
    #result
    uint32 result

    ---
    #feedback
    uint32 result
    ```
    
    For allowing the use of this custom action on your ROS machine side insert this action-file e.g. in the `TCP-Connector` repository in the directory `src/ROS-TCP-Endpoint/action`.

    With the use of the Unity-Robotics-Hub you can automatically generate ROS-Messages with the Unity-Editor.
    Generate messages with your previous created file ```PointAt.action``` by clicking on the following path in the Unity-Menubar. ```Robotics -> Generate ROS Messages``` and select your own file and generate corresponding ROS-Message files in C# for Unity (for more information see [Unity-Robotics-Hub, Message Generation](https://github.com/Unity-Technologies/ROS-TCP-Connector/blob/main/MessageGeneration.md)). Select the `Assets/RosMessages/` directory as target.
    
    **Hint:** For fixing some unknown behaviors during the auto-generation process of the ROS action-files. Add the `Register` function into the newly automatically created files:

    ```bash
    #if UNITY_EDITOR
        [UnityEditor.InitializeOnLoadMethod]
    #else
        [UnityEngine.RuntimeInitializeOnLoadMethod]
    #endif
        public static void Register()
        {
            MessageRegistry.Register(k_RosMessageName, Deserialize);
        }
    ```
    <br>

2. **Implement an own ROS-ActionClient for your custom action** <br>
   For allowing **RISE** to communicate with the environment you have to create a custom ROS-ActionClient for your own **BehaviorAction**. For this create a file under ```Assets/Scripts/ROS/ROSActions/Client/```. Example file for a ```PointAtActionClient.cs```:

    ```bash
    using RosMessageTypes.Geometry;
    using RosMessageTypes.Server;

    public class PointAtActionClient : ROSActionClient<PointAtAction, PointAtActionGoal, PointAtActionResult, PointAtActionFeedback, PointAtGoal, PointAtResult, PointAtFeedback>
    {
        public PointStampedMsg point;
        public float roll;

        private void Start()
        {
            this.Initialize(UIGetComponentValues.topicRobotPrefix);
            this.action = new PointAtAction();
            this.goalStatus = new RosMessageTypes.Actionlib.GoalStatusMsg();
        }

        public void PublishGoal(double x, double y, double z, float roll)
        {
            this.point = new PointStampedMsg();
            this.point.point.x = x;
            this.point.point.y = y;
            this.point.point.z = z;
            this.roll = roll;

            this.SendGoal();
        }

        protected override LookattargetActionGoal GetActionGoal()
        {
            this.action.action_goal.goal.point = point;
            this.action.action_goal.goal.roll = roll;

            return this.action.action_goal;
        }

        protected override void OnFeedbackReceived()
        {
            //throw new System.NotImplementedException();
        }

        protected override void OnResultReceived()
        {
            //throw new System.NotImplementedException();
        }

        protected override void OnStatusUpdated()
        {
            //throw new System.NotImplementedException();
        }
    }
    ```
    <br>

3. **Implement the publish function for your Action** <br>
   After creating your ROS-ActionClient and your custom ROS-Message, you have to extend the **BehaviorController** in ```Assets/Scripts/Controller/BehaviorActionController.cs``` by two functions and some references. Add your created ROS-ActionClient as reference in the attributes of the **BehaviorActionController** and implement a new publish function for your action:
   
    ```code
    public void PublishPointAt(BehaviorActionPointAt actionPointAt)
    {
        ReflectBehaviorActionAttributes(actionPointAt);

        if (uiComponentController.IsDebugMode())
        {
            Debug.Log(actionPointAt.DebugBehaviorAction());
        }
        else
        {
            double pointx = actionPointAt.x;
            double pointy = actionPointAt.y;
            double pointz = actionPointAt.z;
            float roll = actionPointAt.roll;

            pointAtTargetActionClient.PublishGoal(pointx, pointy, pointz, roll);
        }
    }
    ```

    If necessary, here you can also add manipulations of your data which is published in the next step to your robot, depending on your own Robot-Wrapper.
    <br>

    In a second step, extending the function which is executed by your `BehaviorAction` by adding your custom action into the function ```PublishTranslatedAction``` in the ```BehaviorActionController``` and call your Publish-Function created before:
    
    ```bash
    case ActionEnum.Action.PointAt:
        PublishPointAt((BehaviorActionPointAt)behaviorAction);
        break;
    ```
    
    <br>

4. **Setting up your Scene and extending your own Robot-Wrapper** <br>
   By running your system and publishing your created action, RISE will publish the action on a given topic to ROS. 
   Don't forget to add your ROS-ActionClient into your Unity-Scene on the hierarchy into the Gameobject ```RISE-ManagementScene -> ROSCommunication -> ROSActionClients```.
   Here, you can also define a topic as **Action Name** in the inspector. Reference this new added ROS-ActionClients to the **BehaviorActionController** in the hierarchy ```RISE-ManagementScene -> ApplicationController -> ActionController```.
   Make sure, you create any subscriber in your Robot-Wrapper to receive this new message and call your robot-api to execute corresponding behaviors. You can also read the next chapter, which describes how to use own robots with **RISE**.