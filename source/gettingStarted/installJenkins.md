> **_WARNING:_** This installation instructions are deprecated. To make RISE easier to use, a Docker image is now provided with all the necessary functionality ([RISE in Docker](docker.md)).


Install for productive use with Jenkins (deprecated!)
=====================================================

The RISE project is installable via [Research Development Toolkit (RDTK)](https://rdtk.github.io/documentation/index.html), a tool that helps software developers to work on complex, heterogeneous, software-intensive systems by leveraging the Jenkins CI platform.

> **_NOTE:_** For **internal developers from our working group**, follow the installation instructions for another distribution which includes our virtual agent Floka ([install mas-rise-floka-naoqi.distribution](https://gitlab.ub.uni-bielefeld.de/flobi_floka/documentation/-/blob/main/mas-rise-floka-naoqi.md)).

The following steps must be carried out for the installation via the RDTK:

1. Set up the RDTK environment described here: [RDTK getting started](https://rdtk.github.io/documentation/getting_started/install.html)
2. Install the dependencies for the [RISE distribution](https://opensource.cit-ec.de/projects/citk/repository/revisions/master/entry/distributions/mas-rise-naoqi.distribution) ``mas-rise-naoqi.distribution`` within your RDTK environment. To get the required dependencies for your platform (example Ubuntu) run the following:

```bash
cd $RDTK_ROOT
   ./build-generator platform-requirements \
       --platform ubuntu                   \
       citk/distributions/mas-rise-naoqi.distribution
```
The printed platform dependencies must be installed system-wide (without libboost-signals):

```bash
sudo apt install ...
```

Furthermore, **Unity 2021.3.23f1** is needed. The easiest way is, to install the **Unity Hub** [https://unity.com/download](https://unity.com/download). The Unity Hub will be able to select and install the needed version of Unity by itself. 

3. Start Jenkins:
```bash
$RDTK_ROOT/jenkins/start_jenkins
```


4. Generate the build jobs for the distribution:
```bash
cd $RDTK_ROOT
./build-generator generate                         \
    -u USER_NAME_CHANGE_ME                         \
    -p PW_CHANGE_ME                                \
    -D 'view.create?=true'                         \
    -D view.name='RISE'                            \
    citk/distributions/mas-rise-naoqi.distribution \
    -D unity.dir='LOCATION_TO_YOUR_UNITY_DIR_/Hub/Editor2021.3.23f1/Editor'
```
5. If you reload [https://localhost:8080](https://localhost:8080) you should see newly generated jobs. In order to build and deploy your distribution find a job named **orchestration-toolkit-mas-rise-naoqi** and trigger it using the stopwatch icon.



After the bild, everything needed is installed in the distribution volume ``/vol/rise/``. To start the necessary components, the [vdemo](https://code.cor-lab.de/projects/vdemo) script ``vdemo_rise-naoqi-localhost.sh`` in ``/vol/rise/../bin`` and click on start_all.
