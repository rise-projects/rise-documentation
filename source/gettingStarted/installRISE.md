> **_WARNING:_** This installation instructions are deprecated. To make RISE easier to use, a Docker image is now provided with all the necessary functionality ([RISE in Docker](docker.md)).


# Installation of RISE via Jenkins (deprecated!)

The distributions `mas-rise-naoqi.distribution` is included in the CITK (https://opensource.cit-ec.de/projects/citk).
This distribution is build für the working group MAS and includes currently the software **RISE** for controlling the robots, **NAO** and **Pepper** on **Ubuntu** systems!

> **_NOTE:_** For **internal developers from our working group**, follow the installation instructions for another distribution which includes our virtual agent Floka ([install mas-rise-floka-naoqi.distribution](https://gitlab.ub.uni-bielefeld.de/flobi_floka/documentation/-/blob/main/mas-rise-floka-naoqi.md)).

<br>

## 1. Installation of Java

Installing java versions for running jenkins and building some projects:
For building the projects, Java8 is required.
For running jenkins: Java11 or higher is needed.

```sh
sudo apt-get install --no-install-recommends openjdk-8-jdk
sudo apt-get install openjdk-11-jdk
```

<br>

## 2. Installation of ROS

Follow the instructions for installing **ROS Noetic** on your **Ubuntu** device.

> **_Link:_** http://wiki.ros.org/Installation/Ubuntu


<br>

## 3. Installation of Jenkins

For the installation of Jenkins and to get the CITK repositories follow the instructions on:
> **_Link:_** https://rdtk.github.io/documentation/getting_started/install.html

Or follow directly the instructions here:
```sh
sudo apt-get install curl git
export RDTK_ROOT=$HOME/RDTK
echo "export RDTK_ROOT=$HOME/RDTK" >> ~/.bashrc
mkdir -p $RDTK_ROOT
```


Download the **build-generator** (`build-generator-0.34-x86_64-ubuntu-focal`) into your Downloads folder:
> **_Link:_** https://github.com/RDTK/generator/releases/tag/release-0.34.2

Move the downloaded build-generator into your RDTK directory and change permissions:
```sh
cd $RDTK_ROOT
mv ~/Downloads/build-generator-0.34-x86_64-ubuntu-focal $RDTK_ROOT
chmod u+x build-generator-0.34-x86_64-ubuntu-focal
```


Installing Jenkins with the following commands, but change **user-name**, **password** and **email**:
```sh
sudo apt-get install --no-install-recommends openjdk-8-jdk
cd $RDTK_ROOT
ln -s build-generator-0.34-x86_64-ubuntu-focal build-generator

./build-generator install-jenkins \
    -u "USER_NAME_CHANGE_ME" \
    -p "PW_CHANGE_ME" \
    -e name@foo.com \
    jenkins

cd jenkins
./start_jenkins
```

Clone the CITK Projects into your home directory:
```sh
cd $RDTK_ROOT
git clone https://opensource.cit-ec.de/git/citk
```


Check the dependencies for the mas-rise-naoqi.distribution:
```sh
cd $RDTK_ROOT
./build-generator platform-requirements \
    --platform ubuntu \
     citk/distributions/mas-rise-naoqi.distribution
```

Copy the requierements without the "libboost-signals-dev" package into your terminal and install them:
```sh
sudo apt-get install buffer cmake g++ git git-lfs inotify-tools iwidgets4 libboost-date-time-dev  \
  libboost-filesystem-dev libboost-iostreams-dev libboost-program-options-dev  \
  libboost-regex-dev libboost-system-dev  \
  libboost-thread-dev make openssh-client openssh-server  \
  ros-noetic-actionlib-msgs ros-noetic-genmsg ros-noetic-message-generation  \
  screen tclx8.4 tk xterm
```


Generate the the distribution and change **user-name** and your **password** (same as above), for example.
There might appear some small/simple errors but nevertheless, if the build-generator successfully finishes everything should be fine.

```sh
cd $RDTK_ROOT
./build-generator generate \
    -u "USER_NAME_CHANGE_ME" \
    -p "PW_CHANGE_ME" \
    -D 'view.create?=true' \
    -D view.name='MAS-RISE-NaoQi' citk/distributions/mas-rise-naoqi.distribution
```

Create a installation volume for your distribution:
```sh
sudo mkdir /vol
sudo chmod 777 /vol
```

<br>
  
## 4. Configure Jenkins

If not done already, start jenkins:
```sh
cd ~/RDTK  
./jenkins/start_jenkins 
```

Then go to a browser and find Jenkins on https://localhost:8080  
Login with your jenkins USERNAME and PASSWORD which was also used for the build-generator.
Navigate to "Manage Jenkins" > "Global Tool Configuration".

In section "JDK" add a JDK with name "Java8". Uncheck the "Install automatically" checkbox and add your installation path of Java 8 as "JAVA_HOME".  
- Hint: Search for the JDK-installation path on your system via `which java`. To check if the returned path (e.g. /usr/bin/java) is the actual location of your JDK or rather a symbolic link to the actual location, use `ls -l /usr/bin/java`.  
- If you receive something like `/usr/bin/java -> /etc/alternatives/java`, follow the symlink in the same manner until you reach the JDK location (e.g. usr/lib/jvm/java-8-openjdk-amd64/)
Don't forget to apply your changes.

<br>

## 5. Building the Distributions via Jenkins

If not done already, start jenkins:
```sh
cd ~/RDTK  
./jenkins/start_jenkins  
```

Then go to a browser and find Jenkins on https://localhost:8080  
Login with your jenkins **USERNAME** and **PASSWORD**  

Find the "orchestration-toolkit-*" project and build it.  
If you can't build be sure to be logged in to jenkins!  
Building all projects can take a while.  
Refresh your browser from time to time to update the jenkins view.  
If a project fails to build you can check the console output via jenkins for debugging.


<br>

## After Installation, start the System

To start the system controller, use the following shell script:

```sh
cd /vol/rise/mas-rise-naoqi/bin
./vdemo_rise-naoqi.sh
```

For configure your environment use the configuration file. You can change path to configuration files, ip addresses and other system communication settings.

```sh
cd /vol/rise/mas-rise-naoqi/etc/vdemo_scripts
vim rise-naoqi-config.sh
```

> **_NOTE:_** For **internal developers from our working group**: You have to adapt the paths yourself, the documentation is designed for a different distro.
> A first documentation can be seen under: [Start vdemo](https://gitlab.ub.uni-bielefeld.de/flobi_floka/documentation/-/blob/main/system_start.md).
> The documentation gives you first insights of how to start the builded environment.




## Troubleshooting

1. Since a newer version of Jenkins, orchestration may fail with an error related to the Groovy script, such as:

```bash
Build step 'Execute system Groovy script' marked build as failure
Finished: FAILURE
```
**Solution:** Go to “Manage Jenkins” -> “In Process Script Approval” and click on the “Clear Approvals” button above the text field. Start the Orchestration again. Afterwards the error should not appear again.

|             ![platzhalter](../images/jenkins_scriptapprovals.png)              |
|:--------------------------------------------------------------------------:|
| *Jenkins Configuration for the script approvals* |

<br>
<br>

***If you have any further problems installing RISE, do not hesitate to contact us: [Contact Information](../contact.md)***