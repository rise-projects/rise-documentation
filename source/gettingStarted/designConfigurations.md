Design configurations
=====================

## Communication Robot Acts

The whole content is surrounded by an `Agent-Dialogs` tag.

#### Attributes

|  Name   | Type | Required |   Information    |
|:-------:|:----:|:--------:|:----------------:|
| `xmlns` | text |   yes    | must be `craXSD` |

#### Sub-tags

|   Name   |  Type   | Recurrence | Required | Information |
|:--------:|:-------:|:----------:|:--------:|:-----------:|
| `Dialog` | complex | limitless  |   yes    |   one CRA   |

```xml
<Agent-Dialogs
        xmlns="craXSD">
    ...
</Agent-Dialogs>
```

Inside this tag are the `Dialog` tags.

### Dialog tag

#### Attributes

|  Name  | Type | Required |  Information  |
|:------:|:----:|:--------:|:-------------:|
| `name` | text |   yes    | names the CRA |

#### Sub-tags

|    Name     |  Type   | Recurrence | Required |            Information             |
|:-----------:|:-------:|:----------:|:--------:|:----------------------------------:|
|  `Speech`   | complex | limitless  |    no    |      lets robot say a message      |
|  `LookAt`   | complex | limitless  |    no    | lets robot look at a certain point |
|  `Emotion`  | complex | limitless  |    no    |   lets robot express an emotion    |
| `Animation` | complex | limitless  |    no    |     lets robot do an animation     |
|    `Wait`   | complex | limitless  |    no    |     lets robot do waiting          |
| `SocialExpression` | complex | limitless  |    no    |     lets robot do an social expression |

```xml
<Agent-Dialogs>
    <Dialog name = "Greeting">
        <Speech>
            <actionID>1</actionID>
            <message>Hello, my name is Floka!</message>
        </Speech>
        <LookAt>
            <actionID>2</actionID>
            <waitForActions>1</waitForActions>
            <pointx>1.0</pointx>
            <pointy>3.0</pointy>
            <pointz>0.0</pointz>
            <roll>1.0</roll>
        </LookAt>
        <Emotion>
            <actionID>3</actionID>
            <waitForActions>1</waitForActions>
            <value>10</value>
            <duration>1000</duration>
        </Emotion>
        <Animation>
            <actionID>4</actionID>
            <waitForActions>2,3</waitForActions>
            <target>1</target>
            <repetitions>1</repetitions>
            <duration_each>1000</duration_each>
            <scale>5</scale>
        </Animation>
        <Wait>
            <actionID>5</actionID>
            <waitForActions>4</waitForActions>
            <duration>1000</duration>
        </Wait>
        <SocialExpression>
            <actionID>6</actionID>
            <waitForActions>5</waitForActions>
            <value>1</value>
            <duration>1000</duration>
            <intensity>1000</intensity>
        </SocialExpression>
    </Dialog>
</Agent-Dialogs>
```

In each Dialog tag must be at least a
`Speech`, `LookAt`, `Emotion`, `Animation`, `Wait` or `SocialExpression` tag.


### Speech

> *The robot says a message.* The speech message will be divided into sentences, enabling the utilization of lengthy, well-structured text sentences while generating accurately produced synthesized speech utterances.

#### Sub-tags

|       Name       |  Type  | Required |                     Information                      |
|:----------------:|:------:|:--------:|:----------------------------------------------------:|
|    `actionID`    | number |   yes    |              id that refers to this tag              |
| `waitForActions` | number |    no    |   can contain a list of `actionID`, exp.: `1,3,4`    |
|    `message`     | mixed  |    no    | makes the robot say the content of the `message` tag |


#### Message

##### Sub-tags

|    Name    |  Type   | Recurrence | Required |            Information             |
|:----------:|:-------:|:----------:|:--------:|:----------------------------------:|
| `boundary` | complex | limitless  |    no    | a pause inside the message content |

The message is a mixed type of text and possible `boundary` tags in between.

##### Boundary

###### Attributes

|    Name    |  Type  | Required |   Information   |
|:----------:|:------:|:--------:|:---------------:|
| `duration` | number |   yes    | in milliseconds |

Each boundary tag has a duration attribute that specifies
the amount of time the text is being paused for.

```xml
        <Speech>
            <actionID>1</actionID>
            <waitForActions>2</waitForActions>
            <message>Hello!<boundary duration="1000"/>How are you?</message>
        </Speech>
```

### LookAt

> *The robot looks at a certain point.* (Decimal seperator is '.')

#### Sub-tags

|       Name       |  Type   | Required |                   Information                   |
|:----------------:|:-------:|:--------:|:-----------------------------------------------:|
|    `actionID`    | number  |   yes    |           id that refers to this tag            |
| `waitForActions` | number  |    no    | can contain a list of `actionID`, exp.: `1,3,4` |
|     `pointx`     | decimal |    no    |        x-coordinate of the target-point         |
|     `pointy`     | decimal |    no    |        y-coordinate of the target-point         |
|     `pointz`     | decimal |    no    |        z-coordinate of the target-point         |
|      `roll`      | decimal |    no    |  velocity of the motion movements (0.0 - 1.0)   |

```xml
        <LookAt>
            <actionID>1</actionID>
            <waitForActions>2</waitForActions>
            <pointx>4</pointx>
            <pointy>6</pointy>
            <pointz>12</pointz>
            <roll>0.5</roll>
        </LookAt>
```

### Emotion

> *The robot expresses an emotion.*

#### Sub-tags

|       Name       |  Type  | Required |                    Information                    |
|:----------------:|:------:|:--------:|:-------------------------------------------------:|
|    `actionID`    | number |   yes    |            id that refers to this tag             |
| `waitForActions` | number |    no    | can contain a list of `actionID`, exp.: ``1,3,4`` |
|     `value`      | number |    no    |           id that refers to an emotion            |
|    `duration`    | number |    no    |     how long the robot expresses the emotion      |

```xml
        <Emotion>
            <actionID>1</actionID>
            <waitForActions>2</waitForActions>
            <value>4</value>
            <duration>1000</duration>
        </Emotion>
```


### Animation

> *The robot does an animation.*

#### Sub-tags

|       Name       |  Type   | Required |                    Information                    |
|:----------------:|:-------:|:--------:|:-------------------------------------------------:|
|    `actionID`    | number  |   yes    |            id that refers to this tag             |
| `waitForActions` | number  |    no    | can contain a list of `actionID`, exp.: ``1,3,4`` |
|     `target`     | number  |   yes    |     preset number of an predefined animation      |
|  `repetitions`   | number  |    no    |        how often the animation is repeated        |
| `duration_each`  | number  |    no    |          how long each repetition lasts           |
|     `scale`      | decimal |    no    |   velocity of the motion movements (n), n∊[0, ∞)     |

```xml
        <Animation>
            <actionID>1</actionID>
            <waitForActions>2</waitForActions>
            <target>4</target>
            <repetitions>1</repetitions>
            <duration_each>1000</duration_each>
            <scale>5</scale>
        </Animation>
```

### Wait

> *The CRA for a robot is waiting.*

#### Sub-tags

|       Name       |  Type  | Required |                    Information                    |
|:----------------:|:------:|:--------:|:-------------------------------------------------:|
|    `actionID`    | number |   yes    |            id that refers to this tag             |
| `waitForActions` | number |    no    | can contain a list of `actionID`, exp.: ``1,3,4`` |
|    `duration`    | number |   yes    |        duration in milliseconds for waiting       |

```xml
        <Wait>
            <actionID>1</actionID>
            <duration>1000</duration>
        </Wait>
```

### SocialExpression

> *The robot express a social state of behavior.*

#### Sub-tags

|       Name       |  Type  | Required |                    Information                    |
|:----------------:|:------:|:--------:|:-------------------------------------------------:|
|    `actionID`    | number |   yes    |            id that refers to this tag             |
| `waitForActions` | number |    no    | can contain a list of `actionID`, exp.: ``1,3,4`` |
|     `value`      | number |    yes    |           id that refers to an social behavior   |
|    `duration`    | number |    yes    |     how long the robot expresses the behavior    |
|    `intensity`   | number |    yes    | how visually present the robot expresses the behavior |


```xml
        <SocialExpression>
            <actionID>1</actionID>
            <value>1</value>
            <duration>1000</duration>
            <intensity>1</intensity>
        </SocialExpression>
```

<br/><br/>
____
<br/><br/>

## Interaction Rules

The whole content is surrounded by an `InteractionRules` tag.

#### Attributes

|  Name   | Type | Required |   Information   |
|:-------:|:----:|:--------:|:---------------:|
| `xmlns` | text |   yes    | must be `irXSD` |

#### Sub-tags

|       Name        |  Type   | Recurrence | Required | Information |
|:-----------------:|:-------:|:----------:|:--------:|:-----------:|
| `InteractionRule` | complex | limitless  |   yes    |   one IR    |

```xml
<InteractionRules
        xmlns="irXSD">
    ...
</InteractionRules>
```

Inside this tag are the `InteractionRule` tags.

### InteractionRule

#### Attributes

|    Name    |  Type  | Required |                 Information                 |
|:----------:|:------:|:--------:|:-------------------------------------------:|
|   `name`   |  text  |   yes    |                 names an IR                 |
| `initial`  |  text  |   yes    |    state the interaction rule start with    |
| `priority` | number |    no    | initial priority value for stack processing |

#### Sub-tags

|  Name   |  Type   | Recurrence | Required |          Information          |
|:-------:|:-------:|:----------:|:--------:|:-----------------------------:|
| `State` | complex | limitless  |   yes    | different steps in a sequence |

```xml
    <InteractionRule name="ExampleInteractionRule" initial="State 1">
        ...
    </InteractionRule>
```

In each InteractionRule tag must be at least one `State` tag.


### State

#### Attributes

| Name |  Type  | Required |         Information          |
|:----:|:------:|:--------:|:----------------------------:|
| `id` | number |   yes    | id that refers to this state |

#### Sub-tags

|     Name     |  Type   | Recurrence | Required |              Information               |
|:------------:|:-------:|:----------:|:--------:|:--------------------------------------:|
|  `onEntry`   | complex |    once    |   yes    |   what happens when entering a state   |
| `transition` | complex | limitless  |    no    | when to jump to the named target state |

```xml
        <State id="1">
            <onEntry>
                ...
            </onEntry>
            <transition/>
        </State>
```

### OnEntry

#### Sub-tags

|    Name    |  Type   | Recurrence | Required |              Information              |
|:----------:|:-------:|:----------:|:--------:|:-------------------------------------:|
|    `if`    | complex | limitless  |    no    | what happens when a condition is true |
| `function` | complex | limitless  |    no    |        executes named function        |

The `onEntry` tag has to have at least one `if` tag or one `function` tag.


#### If

##### Attributes

|  Name  | Type | Required |           Information           |
|:------:|:----:|:--------:|:-------------------------------:|
| `cond` | text |   yes    | checks if the condition is true |

##### Sub-tags

|    Name    |   Type   | Recurrence | Required |                 Information                 |
|:----------:|:--------:|:----------:|:--------:|:-------------------------------------------:|
| `function` | complex  | limitless  |   yes    |           executes named function           |
|  `elseif`  | complex  | limitless  |    no    | another condition after the first was false |
|   `else`   | complex  |    once    |    no    | what happens when every condition is false  |

```xml
            <onEntry>
                <if cond="exampleCondition">
                    <function/>
                    <elseif cond="exampleElseCondition">
                        <function/>
                    </elseif>
                    <else>
                        <function/>
                    </else>
                </if>
                <function/>
            </onEntry>
```


##### Else-if

###### Attributes

|  Name  |  Type  | Required |           Information           |
|:------:|:------:|:--------:|:-------------------------------:|
| `cond` |  text  |   yes    | checks if the condition is true |

###### Sub-tags

|    Name    |  Type   | Recurrence | Required |       Information       |
|:----------:|:-------:|:----------:|:--------:|:-----------------------:|
| `function` | complex | limitless  |   yes    | executes named function |


##### Else

###### Sub-tags

|    Name    |  Type   | Recurrence | Required |       Information       |
|:----------:|:-------:|:----------:|:--------:|:-----------------------:|
| `function` | complex | limitless  |   yes    | executes named function |

##### Conditions

You can use multiple logic expressions to form a condition.

###### Possible Operations

|  Logic Operators  | Arithmetic Operators | Text Operators |
|:-----------------:|:--------------------:|:--------------:|
| or / &#124;&#124; |          +           |    contains    |
|      xor / ^      |          -           |  starts with   |
|     and / &&      |          *           |   ends with    |
|        ==         |          /           |       ==       |
|        !=         |          ^           |       !=       |
|        >=         |                      |       >=       |
|         >         |                      |       >        |
|        <=         |                      |       <=       |
|         <         |                      |       <        |
|      not / !      |                      |                |

Variables can be used by putting the variable name between two square brackets.

```xml
                <if cond="10 != [ten]">
                    <function/>
                    <elseif cond="[robotName] == NAO">
                        <function/>
                    </elseif>
                    <elseif cond="(42/7 != 6) and ([personName] starts with Alex)">
                        <function/>
                    </elseif>
                </if>
```

#### Function

##### Attributes

|             Name             | Type | Required |                  Information                   |
|:----------------------------:|:----:|:--------:|:----------------------------------------------:|
| `startCommunicationRobotAct` | text |    no    | starts a CRA with the given name / starts one random CRA from given list of CRA |
| `stopCommunicationRobotAct` | text |    no    | stops a CRA with the given name |
| `startInteractionRule` | text |    no    | start an IR with the given name |
| `stopInteractionRule` | text |    no    | stops an IR with the given name |
|      `raiseEventTopic`       | text |    no    | raises an event that can trigger an transition |
|        `assignValue`         | text |    no    |         assigns a value to a variable          |

The function tag must have exactly one of the possible attributes from above.

```xml
            <onEntry>
                <function startCommunicationRobotAct = "ExampleDialog" />
                <function startCommunicationRobotAct = "[ExampleDialog1, ExampleDialog2]" />
                <function startInteractionRule = "ExampleInteractionRule" />
                <function raiseEventTopic = "/No_Interaction"/>
                <function assignValue = "expVariable = 'Hello'" />
                <function stopInteractionRule = "ExampleInteractionRule" />
            </onEntry>
```

### Transition

#### Attributes

|     Name     | Type | Required |            Information             |
|:------------:|:----:|:--------:|:----------------------------------:|
| `eventTopic` | text |   yes    | event that triggers the transition |
|   `target`   | text |   yes    |            target state            |

```xml
    <InteractionRule>
        <State id="1">
            <onEntry>
                <if cond="exampleCondition">
                    <function startCommunicationRobotAct = "ExampleDialog" />
                    <else>
                        <function raiseEventTopic = "/No_Interaction"/>
                    </else>
                </if>
            </onEntry>
            <transition eventTopic="/ExampleDialog_end" target="State 2"/>
            <transition eventTopic="/No_Interaction" target="State 3"/>
        </State>
        <State id="2">
            ...
        </State>
        <State id="3">
        ...
        </State>
    </InteractionRule>
```

<br/><br/>
____
<br/><br/>

## Working Memory

Unlike IR and CRA configruartion, the Working Memory is represented as JSON in a .json file. 


```JSON
{"WorkingMemory":
  [
    {"Participant_Name" : "Tom"},

    {"H_x_pos" : 0.0},
    {"H_y_pos" : 0.0},
    {"H_z_pos" : 1.0},

    {"Repetitions" : 4},

    {"HasGlasses" : true},
  ]
}

```

It is important that the key-value pairs are present as a list, seperated by ´,´ within a `WorkingMemory` object.

#### Datatypes

Due to the Json format, any simple Json object is accepted.

| Type          | Example                                               |
|:--------------:|:------------------------------------------------------|
| String         |  {"String_example" : "some 'important' example text"} |
| Double            |  {"Double_example" : 42.13} |
| Integer       |   {"Integer_example" : 7}```   |
| Boolean       |   {"Bool_example" : false}   |




#### Calculating with Variables

The WM allows to calculate variables and to assign them. The capabilites of this functions related to the [compute function from C#](https://learn.microsoft.com/de-de/dotnet/api/system.data.datatable.compute?view=net-7.0). The following instructions are allowed:

| Logic          | Example                                               |
|:--------------:|:------------------------------------------------------|
| assign         | "variable = variable2"                                |
| add            | "variable = variable + 1"                             |
| equation       | "variable = variable / (5 * constant) + repetition"   |

This calculations can be done throw the [RISE UI](../user-interface.md), inside an IR or from a [external Node](../tutorials/tutorialNewComponents.md).
<br/><br/>
____
<br/><br/>

## Variables

Variables which are already established can be used in tag-contents 
by putting the variable name between two percent signs.

```xml
            <message>
                Hello %personName%, I am %robotName%.
                <boundary duration="1000"/>
                How are you?
            </message>
```