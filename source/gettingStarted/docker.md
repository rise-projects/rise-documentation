# Use RISE via Docker

## Install Docker

The current documentation allows you to use the docker images hosted by our working group. The images have to use hardware acceleration. These instructions describe the process of installing all relevant components for an Ubuntu computer with a nvidia GPU only.

On Ubuntu, setup Dockers apt repository.

```bash
sudo apt-get update
sudo apt-get install ca-certificates curl gnupg
sudo install -m 0755 -d /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
sudo chmod a+r /etc/apt/keyrings/docker.gpg

# Add the repository to Apt sources:
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
  $(. /etc/os-release && echo "$VERSION_CODENAME") stable" | \
  sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update
```

Install docker locally.

```
sudo apt-get install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
```

Verify docker is installed correctly.

```
sudo docker run hello-world
```

### (Optional) Executing Docker without sudo

By default, only the root user can run Docker commands. To run Docker commands without the sudo prefix ([source](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-20-04#step-2-executing-the-docker-command-without-sudo-optional)), you need to add your user to the Docker group:

```
sudo usermod -aG docker ${USER}
su - ${USER}
```

**Close the current Terminal!**

Verify, that you can run a docker without sudo

```
docker run hello-world
```

## Install Nvidia Container Toolkit

For some applications or calculation you need support of your graphic cards. The [Nvidia Container Toolkit](https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/latest/install-guide.html) allows you to use hardware ressources of your GPU in docker itself.

```
curl -fsSL https://nvidia.github.io/libnvidia-container/gpgkey | sudo gpg --dearmor -o /usr/share/keyrings/nvidia-container-toolkit-keyring.gpg \
  && curl -s -L https://nvidia.github.io/libnvidia-container/stable/deb/nvidia-container-toolkit.list | \
    sed 's#deb https://#deb [signed-by=/usr/share/keyrings/nvidia-container-toolkit-keyring.gpg] https://#g' | \
    sudo tee /etc/apt/sources.list.d/nvidia-container-toolkit.list
```

```
sudo sed -i -e '/experimental/ s/^#//g' /etc/apt/sources.list.d/nvidia-container-toolkit.list
```

```
sudo apt-get update
sudo apt-get install -y nvidia-container-toolkit
```

Configure nvidia container toolkit in docker:

```
sudo nvidia-ctk runtime configure --runtime=docker
sudo systemctl restart docker
```

Use your GPU in docker for example with this command as an example:

```
xhost +

docker run -it --net=host --gpus all \
	--env="NVIDIA_DRIVER_CAPABILITIES=all" \
	--env="DISPLAY" \
	--env="QT_X11_NO_MITSHM=1" \
	--volume="/tmp/.X11-unix:/tmp/.X11-unix:rw" \
	osrf/ros:noetic-desktop-full \
	bash -it -c "roslaunch gazebo_ros empty_world.launch"
```

### NVIDIA GPU troubleshooting

If you experience an error with RISE closing on startup with a _segmentation fault_ check the _NVIDIA X Server Settings_ application and make sure it is set to _NVIDIA (Performance Mode)_. This setting can toggle itself on updates.

| NVIDIA GPU working | NVIDIA GPU not working |
| ------ | ------ |
|![GPU working and enabled](../_static/images/nvidia-x-server-settings_gpu-enabled.png)|![GPU working and enabled](../_static/images/nvidia-x-server-settings_gpu-disabled.png)|


## Start RISE in Docker

Download the docker image for RISE from the [official docker registry](https://hub.docker.com/r/unibimas/rise)

```
docker pull unibimas/rise:latest
```

Start the container:

```
# Allow X11 connections from any client to host
xhost +

# Check display number from variable with "echo $DISPLAY"
# Environment variable to pass X11 applications
if [ "$DISPLAY" == "" ]; then
    echo "-> ERROR: No display variable found"
    exit 0
fi
MY_DISPLAY_HOST=":$(cut -d ':' -f2 <<< $DISPLAY)"

# Start docker container detached
docker run --platform linux/amd64 -d --name rise --net bridge --rm --gpus all \
    -e NVIDIA_DRIVER_CAPABILITIES=all \
    -e QT_X11_NO_MITSHM=1 \
    -e DISPLAY=$MY_DISPLAY_HOST \
    -v /tmp/.X11-unix:/tmp/.X11-unix \
    "unibimas/rise:latest";
```

Connect to the container for an interactive terminal session.

```
docker exec -it rise /bin/bash
```

Start vdemo and components for RISE.

```
cd /vol/distribution/bin/
./vdemo_rise-naoqi.sh
```

The ROS-node management GUI (vdemo) starts in a graphical user interface. Click on Start All Components and a roscore, the tcp-connector and RISE starts.

In case you want to connect a external robot to RISE inside the docker: Forward ports from Host to Docker container with `-p <PORT>:<PORT>`:

```
docker run --platform linux/amd64 -d --name rise --net bridge --rm --gpus all \
    -p <PORT>:<PORT> \
    -e NVIDIA_DRIVER_CAPABILITIES=all \
    -e QT_X11_NO_MITSHM=1 \
    -e DISPLAY=$MY_DISPLAY_HOST \
    -v /tmp/.X11-unix:/tmp/.X11-unix \
    "unibimas/rise:latest";
```