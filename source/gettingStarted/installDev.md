Install for developing in Unity
===============================

To enable the personal development on the RISE-Core, the whole project is open source. In this chapter, the process to install RISE with the configurations and a TCP-Connector will be explained step by step. This can be used to adapt the functionalities or install it without Jenkins.

The Unity application is able to run on Windows Mac or Linux. 
ROS is only supported for Ubuntu and Debian. Therefore, the TCP-Connector, to connect Unity with ROS, and any Robot Wrapper needs to be installed on an Ubuntu or Debian Machine. It is possible to cover this by using a virtual machine with, e.g., Ubuntu on a Windows machine. The explanation therefore is not covered here.

## Install and run the RISE-Core in Unity

First, **Unity 2022.3.8f1** is needed. The easiest way is, to install the **Unity Hub** [https://unity.com/download](https://unity.com/download). The Unity Hub will be able to select and install the needed version of Unity by itself. 
The next step is to clone the RISE-Core project from GitLab:

```bash
  git clone https://gitlab.ub.uni-bielefeld.de/rise-projects/application/rise-core
```
When opening the RISE-core folder in the Unity Hub, a warning appears, that recommends installing the needed unity version. By click the  **Install Version Unity 2022.3.8f1** button, the needed version will be installed. Afterwards the rise-core project can be opened.

To load the right scene, open in Unity under Assets/Scenes the **RISE-ManagementScene** and switch from the Scene View to the Game View in tab selection on the top.
The  [RISE UI](../user-interface.md) should be visible.
When entering the play mode in Unity, no Communication Robot Acts **(CRA)** or Interaction Rules **(IR)** are visible and under setting *Connection refused* is displayed. The RISE-Core needs a tcp connection to access the Roscore and to display the IRs and CRAs the path to the configuration files is needed.

Let's start with adding the configuration files for the CRA, IR and the Working Memory **(WM)**.

### Adding configurations to the RISE-Core
To be able to integrate the configuration files into the RISE-Core, first they need to cloned from the repository. 

```bash
  git clone https://gitlab.ub.uni-bielefeld.de/rise-projects/application/rise-configurations
```

Following the full path to the cloned folder will be called ***your_path*/rise-configurations**
In this repository are example configurations for CRAs IRs and the WM and need to be adjusted or replaced.

After again entering the play mode in Unity, the Settings can be unfolded by clicking the Setting button.
The input boxes **ROOT** is empty. Enter your path to the configurations repository from your local machine here and press the reload button to load all possible configurations from the repository. You can see parsed category and projects in the dropdowns below.

#### Creating a JSON startup config
When restarting the play mode, this configuration will be lost. Therefor it is handy to create a JSON file, to automatically loads the given directory or category and project on start.

In the hierarchy window, normally on the left side, the *ApplicationController* needs to be unfolded and *SetupConfig* selected.
In the Inspector, normally on the right side, the *Start Up Configs (Script)* appears. 

In this inspector view the different directory paths as well as the ExecutionMode, the used RobotName and the IP Address to the RosCore can be entered.
After typing in the Path to the root respository, by pressing the *Create JSON by Settings* button, an args.data will be created in the Assets folder. 

When the play mode is started now, the path is parsed automatically. When under Settings the Debug mode is selected, it is already possible to start and test the example CRAs and IRs by pressing the play sign. In the Debug mode, no connection to a RosCore or Robot is needed. The e.g. *speech has ended* feedback will be simulated.

## Enabling the ROS connection

As the next step, a connection between ROS and Unity is needed. For that, a slightly changed version of the ROS TCP Connector provided by Unity-Technologies is used ([https://github.com/Unity-Technologies/ROS-TCP-Connector](https://github.com/Unity-Technologies/ROS-TCP-Connector). **Original script, do not use for RISE!**).

Because ROS is only supported by Ubuntu and Debian, the following steps need to take place in one of that operating systems. This Tutorial is done for Ubuntu 20.04 wit ROS Noetic. Other systems are not tested.
As first step, ROS needs to be installed on that machine. 

Please follow the steps on the official ROS side for the ROS Noetic installation:
[http://wiki.ros.org/noetic/Installation/Ubuntu](http://wiki.ros.org/noetic/Installation/Ubuntu)

### Creating a catkin Workspace
When ROS is successfully installed, the next step is to create a catkin workspace, where the *TCP-Connector*, *Robot Wrapper* or any used node can be built:
```bash
  source /opt/ros/noetic/setup.bash
  mkdir -p ~/catkin_ws/src
  cd ~/catkin_ws/
  catkin_make
```
The full tutorial with deeper explanations can be found here: [http://wiki.ros.org/catkin/Tutorials/create_a_workspace](http://wiki.ros.org/catkin/Tutorials/create_a_workspaced) 

### Building and Starting the TCP-Connector
Next, the adapted TCP-Connector for the RISE-Core can be cloned into the src folder. It is important to use the **include-hlrc-msgs** branch to include the needed ROS messages:

```bash
  cd ~/catkin_ws/src
  git clone -b include-hlrc-msgs https://gitlab.ub.uni-bielefeld.de/rise-projects/application/tcp-connector
  cd ~/catkin_ws/
  catkin_make
```



Before we can start the TCP-Connector, we need to start the Roscore and set up the IP. In the first Terminal, the Roscore is started:
```bash
  roscore
```

In another Terminal, the IP of the Roscore will be defined into the ros parameter server.
To get the current IP of the system, ifconfig can be used.
```bash
  ifconfig
```
For this example the IP *$ROS_IP* is used

Next this IP needs to be written into the ROS parameter server:
```bash
  rosparam set /ROS_IP $ROS_IP
```


Now the TCP-Connector can be started. The Port to RISE-Core is defined to 10001:
```bash
  source ~/catkin_ws/devel/setup.bash
  python3 ~/catkin_ws/src/tcp-connector/src/ROS-TCP-Endpoint/src/ros_tcp_endpoint/ros-unity-endpoint.py 10001
```

A print should be displayed, that the server has started.

**Back to the Unity Rise-Core application.**

After switching to play mode and unfold the settings again, the Roscore IP can be entered and submitted by pressing the checkmark sign. It should turn green. 
Of course, this IP should also be added into the above-mentioned *Stat Up Configs*, where the different directories to the configurations are defined. 


## Adding the Naoqi driver
To be also able to pass the different action, send via ROS to a Robot, the Naoqi driver need to be cloned to the catkin Workspace.
```bash
  cd ~/catkin_ws/src
  git clone https://gitlab.ub.uni-bielefeld.de/rise-projects/robot-wrapper/naoqi-driver.git
```
To be able to build the Naoqi driver, additional repositories from ros-naqoqi are necessary:

```bash
  cd ~/catkin_ws/src
  git clone -b ros https://github.com/ros-naoqi/libqi.git
  git clone -b ros https://github.com/ros-naoqi/libqicore.git
  git clone https://github.com/ros-naoqi/naoqi_bridge_msgs.git
  
```

Now the Naoqi Driver can be build. This will take some time:
```bash
  cd ~/catkin_ws/
  catkin_make 
```

To run the Naoqi driver first the IP of the used Naoqi Robot is needed. For this example the IP *\$NAOQI\_ROBOT\_IP* is used. 
The name of the used Network interface of the Ubuntu machine to replace the *$NAOQI_NETWORK_INTERFACE*, can again be found in *ifconfig*.

```bash
  source ~/catkin_ws/devel/setup.bash
  rosrun naoqi_driver naoqi_driver_node --qi-url=tcp://$NAOQI_ROBOT_IP:9559 --roscore_ip $ROS_IP --network_interface $NAOQI_NETWORK_INTERFACE
```
The Naoqi driver should be running and all the Topics to control the Robot should be visible when running *rostopic list*

## Summary, How to start when everything is prepared.
1. Start a roscore 
```bash
  roscore
```

2. Start the TCP-Connector:

```bash
  source ~/catkin_ws/devel/setup.bash
  rosparam set /ROS_IP $ROS_IP
  python3 ~/catkin_ws/src/tcp-connector/src/ROS-TCP-Endpoint/src/ros_tcp_endpoint/ros-unity-endpoint.py 10001
```

3. Start the RISE-Core Unity Project and enter into the play mode. If no *SetupConfig* for the startup is created, enter the Roscore IP and directory paths to the 3 config folders. Submit every entry with the button on the right of the input box.

4. Start the Naoqi driver:

```bash
  source ~/catkin_ws/devel/setup.bash
  rosrun naoqi_driver naoqi_driver_node --qi-url=tcp://$NAOQI_ROBOT_IP:9559 --roscore_ip $ROS_IP --network_interface $NAOQI_NETWORK_INTERFACE
```
Do not forget to switch the mode in the RISE-Core from debug to *Nao* or *Pepper*.
Now the *CRAs* and *IRs* should be runnable on the Robot with the play sign in RISE-Core UI.

## Creating and running an Executable of the RISE-Core

Of course, it is also possible to create an executable of the RISE-Core project instead of running it in the Unity IDE.
Therefore, open the build dialog in Unity under *File/Build and Run* and select a folder, where the resulting executable should be placed. The UI should open in a new Window. This will be started without predefined config, but the parameters can be passed as parameters. Navigate in a terminal to the selected build folder. It should be a RISE-Core.exe in it. 

From the Terminal you can run the RISE-Core.exe with parameters to pass the configuration. The following commands are possible:

| Parameter <div style="width:85px">property</div>  | Example                                                                        | Description                                                                                 |
|:--------------------------------------------------|:-------------------------------------------------------------------------------|:--------------------------------------------------------------------------------------------|
| `-mode`                                           | `-mode Real`                                                                   | Defines the running type. Possible Values: `Debug` `Real` `Headless`                        |
| `-robot`                                          | `-robot NAO`                                                                   | Name of the used Robot. Predefined are `NAO`, `Pepper` and the not public available `Floka` |
| `-ip`                                             | `-ip 192.168.178.50`                                                           | IP to the Roscore                                                                           |
| `-path_config_root`                                       | `-path_config_root D:\rise-configurations\Configurations\` | Path to the root configuration directory                                                                             |
| `-path_config_category`                                        | `-path_config_category Tutorials`        | Category directory on startup. IR                                                                              |
| `-path_config_project`                                      | `-path_config_project RobotInitiatedGreeting`        | Project directory on startup.                                                                          |


### Example startup command:
```bash
  .\RISE-Core.exe -mode Real -robot NAO -ip 192.168.178.50 -path_config_root 'D:\rise-configurations\Configurations\' -path_config_category 'Tutorials' -path_config_project 'RobotInitiatedGreeting'
```