Graphical User-Interface
========================

<p align="center">
  <img src="https://gitlab.ub.uni-bielefeld.de/rise-projects/rise-documentation/-/raw/development/source/images/gifs/rise_overview_and_control.gif" width="100%" />
</p>

We have developed a graphical user interface to present all processes within RISE in an understandable and comprehensible manner. This allows for easy control of robot behavior and adjustment of internal variables directly via the interface.


## Overview

| <img alt="Graphical User-Interface" src="_static/images/gui_overview.drawio.svg" width="100%" />                                                                                                                                                                                                                                                                                                                                          |
|:--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| *`1` [Settings](#settings-1), `2` [CRAs](#communication-robot-acts-2), `3` [IRs](#interaction-rules-3), `4` [Parameter Server](#parameter-server-4), `5` [Task Scheduler](#task-scheduler-5), `6` [IR States](#interaction-rule-states-6), `7` [CRA States](#communication-robot-act-states-7), `8` [ROS - Domain Task Action Server](#ros-domain-task-action-server-8), `9` Information about the GUI* |


## Settings

| <img alt="Settings" src="_static/images/gui_settings.drawio.svg" width="100%" />                                                                                                                                                                                                               |
|:----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| *`1` [ROS IP](#ros-ip-1), `2` [Directory for CRAs](#directories-2-3-4), `3` [Directory for IRs](#directories-2-3-4), `4` [Directory for Working Memory](#directories-2-3-4), `5` [Mode Menu](#mode-and-robot-5-6), `6` [Robot Menu](#mode-and-robot-5-6)* |

### ROS IP

The IP-address of the system that the TCP-Connector is running on needs to be inserted here.

### Directories

The absolute path of the three folders in `RISE/rise-configurations/Configurations`

* ActiveCommunicationRobotActs
* ActiveInteractionRules
* ActiveWorkingMemory

need to be inserted here.

After changing the directory path the settings (*JSON file*) need to be rebuilt. This can be achieved by pressing the corresponding reload button behind each input field.

### Mode and Robot

The application can be put in different modes:  
The `Debug` mode can be used if no tcp-connection is needed for simulating state-machine transitions only in the GUI.  
The `Real` mode is used when connecting to one of the available robots.  
The `Headless` mode is used to run the application without any use of the GUI (e.g. as a server application).

## Communication Robot Acts

There is a list of all `Communication Robot Acts`.  
To start one the play-button at the end of the item needs to be clicked.  
To stop it the same button, now representing a stop-button, needs to be clicked.  
For a look into the `Communication Robot Act` click on the content.


## Interaction Rules

There is also a list of all `Interaction Rules`.  
It works the same as the `Communication Robot Acts` list listed above.


## Parameter Server

| <img alt="Parameter Server" src="_static/images/gui_parameter-server.drawio.svg" width="100%" />                                                                                                                                                              |
|:-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| *`1` Variable Name, `2` [Type](#type-2), `3` Variable Value, `4` Delete Variable, `5` [Lock/Unlock Variable](#edit-variable-6), `6` [Add Variable](#add-variable-7), `7` Search for Variable* |

### Type

The column *Type* shows the type of the variable.  
There are four types: Integer, Double, Boolean, String

### Edit Variable
<img alt="Edit Variables in RISE" src="_static/images/gui_edit-variable.gif" width="100%" />

The lock-button is used to edit already existing variables.  
The standard is locked. Then the variable cannot be edited.  
With a click the variable is unlocked and can be edited.

### Add Variable
<img alt="Add Variables to RISE" src="_static/images/gui_add-variable.gif" width="100%" />


To create a new variable the add-button needs to be pushed.  
In the new input fields a key and a value have to be entered.  
The confirm-button saves the variable in the parameter server.  
When no input is given the confirm-button cancels the process.  
When trying to save wrong input no variable gets saved and new input can be given.

<div style="padding: 0.5em; background: lightsteelblue; color: dimgrey; border-radius: 0.5em">
<b style="margin: 0 0.5em">!</b> When adding a string variable the value must be written in quotation marks.
</div> <br> <br>


## Task Scheduler

| <img alt="Task Scheduler" src="_static/images/gui_task-scheduler.drawio.svg" width="100%" />                                                                       |
|:------------------------------------------------------------------------------------------------------------------------------|
| *`1` Task ID, `2` [Task Type](#task-type-2), `3` [Execution](#execution-3), `4` Task Name, `5` [Task Status](#task-status-5)* |

### Task Type

The column *Function* shows what kind of task is currently operating.  
One of these task types is `startCommunicationRobotAct` which, according to its name, starts a `Communication Robot Act` with the given `Task Name`.  
Another one is `raiseEventTopic` which raises an event topic that transitions might be waiting for.

### Execution

RISE allows to calls of execution types for internal processes.
The type `Blocking` is used for tasks that cannot be interrupted by the scheduler because another task has become a higher priority. Tasks with the type `None` can also be interrupted because of a priority change in the list.

### Task Status

There are two task status that show how a function is currently operating.  
`Running` means the function is being executed.  
A function has the status `Waiting` when it is waiting for another function to finish running.


## Interaction Rule States

| <img alt="Interaction Rule States" src="_static/images/gui_interaction-rules-states.drawio.svg" width="100%" />                        |
|:-------------------------------------------------------------------------------------------------|
| *`1` [State](#state-1), `2` Forward Transitions, `3` Active Transition, `4` Backward Transition* |

### State

Every `State` has a status indicator which shows whether it is the current status.  
In the header it also shows its state ID which it can be identified with.
In the body are the onEntry and transitions block. It reveals more information when hovered over.


## Communication Robot Act States

| <img alt="Communication Robot Act States" src="_static/images/gui_cra-states.drawio.svg" width="100%" />                                                    |
|:-----------------------------------------------------------------------------------------------------------------------|
| *`1` [Communication Robot Act](#communication-robot-act-1), `2` [WaitForActions](#waitforactions-2), `3` Progress Bar* |

### Communication Robot Act

Every `Communication Robot Act` has a status indicator which shows whether it is the current status.  
In the header it also shows its `actionID` which it can be identified with and its type of `Communication Robot Act`.
In the body the content components are shown. These vary depending on its type (exp. `Speech -> message`).

### WaitForActions

The different `Communication Robot Acts` are connected by a line if one of them is waiting for the other to stop.


## ROS - Domain Task Action Server

| <img alt="ROS - Domain Task Action Server" src="_static/images/gui_ros-domain-task-action-server.drawio.svg" width="100%" />                                               |
|:--------------------------------------------------------------------------------------------------------------------------------------|
| *`1` Action ID, `2` [Task Type](#task-type-2), `3` [Execution](#execution-3), `4` Action Name, `5` [Action Status](#action-status-5)* |

### Action Status

There are some in-process status like:

* PENDING
* ACTIVE
* RECALLING
* PREEMPTING

And end-process status like:

* SUCCEEDED
* REJECTED
* ABORTED
* PREEMPTED
