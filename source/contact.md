Project Responsibility
======================

This project is developed and maintained by the working group..

* [Medical Assistance Systems](https://www.uni-bielefeld.de/fakultaeten/medizin/fakultaet/arbeitsgruppen/assistenzsysteme/)
* Medical School OWL
* Bielefeld University


For any inquiries or questions regarding the project, please reach out to the project contact persons via email:

* André Groß [(contact)](mailto:agross@techfak.uni-bielefeld.de) 
* Christian Schütze [(contact)](mailto:cschuetze@techfak.uni-bielefeld.de) 