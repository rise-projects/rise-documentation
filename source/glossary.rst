.. _glossary:

==========
 Glossary
==========

.. glossary::

   Communication Robot Acts

    CommunicationRobotActs (CRA) describes adjustable robot behaviors. CRA’s consists of a sequence of actions.

   Interaction Rules

    InteractionRules (IR) are a powerfull tool for building reactive and complex behavior. An IR is defined by a state machine graph, where each state contains two processing areas with different functionalities. The first processing area is executed when a state is entered, and it supports several functionalities, including: CommunicationRobotActs, InteractionRule, raiseEventTopic, assignValue

   Working Memory

      Working Memory is used to store variables. They can be saved in XML files and consist of a key and a value.

   Agent Dialogs

      Agent Dialogs are used to set the boundaries of a Communication Robot Act.

   Action IDs

      Action IDs are used to determine the sequence of speeches, emotions, and animations in a Communication Robot Act.