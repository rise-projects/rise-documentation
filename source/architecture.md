Main Architecture
===================


## RISE-Architecture

|     ![platzhalter](images/RISE_Architecture.jpg)     |
|:----------------------------------------------------:|
| *Architecture Diagram with Nodes, ROS-Interface, ..* |


## RISE-Core Components

### ROS-Communication

The **Robot Operating System (ROS)** (see [ROS](https://www.ros.org/)) is a widely-used framework for building robotics software.
One of the key features of ROS is its ability to enable communication between different nodes.
The following components describes the communication mechanism of using ROS in the **RISE**-Architecture.

#### ROS-ActionServer

The **ROS-ActionServer** in this architecture is realized by the structure and rules of the **ActionGoal** lifecycle of a **Simple-ActionServer** (see [ROS-ActionLib](http://wiki.ros.org/actionlib)).
The server has been adapted to use multiple ActionGoals.

The **RISE-Core** system is able to receive **DomainTask-ActionGoals** as input.
This **DomainTasks** represents the bridge between the environment and the system itself.
The **DomainTask-ActionServer** allows external nodes to send a goal message to **RISE** (*server*), which then processes the *goal* and sends *feedback* messages to the nodes (*clients*) as the task progresses.
Once the task is completed, the server sends a result message back to the client.

A **DomainTask** is represented by different *function-types* and *behaviors* ([API Types](api)).
One example of a **DomainTask** could be the starting of an **CommunicationRobotAct** or **InteractionRule**, setting values by keys in the **Working-Memory** or raising **EventTopics** as trigger for **InteractionRules**.

#### ROS-ActionClient

A **ROS-ActionClient** is a node that sends a *goal* message to a server and receives *feedback* and *result* messages during the execution of a long-running, asynchronous task using the **ROS-Action** protocol.
The **ROS-ActionClient** in this architecture represents the output of **CommunicationRobotActs** or **InteractionRules** as Robot-Behaviors.
Each [BehaviorAction](structures) is connected with an **ROS-ActionClient** publisher.
**RISE** will produce **ActionGoals** for **BehaviorActions** which should be executed on a robot and receiving feedback if **ActionGoals** are finished.
The corresponding [Robot-Wrapper](#robot-control) will receive this **ActionGoals** and will process them.


#### ROS-IR-State-Publisher

The **InteractionRule-State-Publisher** is a simple *publisher* for sending the current state of an **InteractionRule** to the environment.
This message contains the unique name of the **InteractionRule** and the name of the running state in the **InteractionRule**.


#### ROS-Memory-Service

The **ROS-Memory-Service** is a communication mechanism that allows an external node (*client*) to request a specific entry in the **Working-Memory** by **RISE** as 
*server*, and receive a response message back from the server using the **ROS-Service** protocol.


### Task-Scheduler

The **TaskScheduler** is a software component that manages and schedules incoming tasks in a queue based on the order of arrival.
The scheduler can handle both blocking and parallel tasks, allowing for efficient use of system resources.
When a task is added to the scheduler, it is appended in the queue at the end.
The scheduler **continuously** checks the queue for tasks that are ready to be executed and starts them as soon as possible.
If a task is blocking, the scheduler will wait for it to complete before moving on to the next task in the queue.
Parallel tasks can be executed simultaneously.
The scheduler also provides status updates and notifications to other nodes in the system, ensuring effective communication and coordination.
The scheduler is the **second component of executing a DomainTask**.
It performs the internal processing of an accepted ROS ActionServer DomainTask.
For the execution of tasks, the scheduler differentiates between the following tasks with the given execution rules:

| DomainTask Function-Types    | Classification          | Execution (priority) Rule                         |
|:-----------------------------|:------------------------|:--------------------------------------------------|
| `startCommunicationRobotAct` | *Time-critical* Task    | **Appending** a new CRA to the scheduler's queue. |
| `stopCommunicationRobotAct`  | *Time-independent* Task | **Immediate** stop of a CRA.                      |
| `startInteractionRule`       | *Time-independent* Task | **Immediate** start of an InteractionRule.        |
| `stopInteractionRule`        | *Time-independent* Task | **Immediate** stop of an InteractionRule.         |
| `raiseEventTopic`            | *Time-independent* Task | **Immediate** raise of an internal EventTopic.    |
| `assignValue`                | *Time-independent* Task | **Immediate** set of an memory in the memory.     |


### Static-Robot Behavior Processing

The execution of **CommunicationRobotActs** are described as generated output of RISE on the robot for the environment.
This CRAs are handled by an **CommunicationRobotAct-Controller** and can be executed/controlled/stopped via [API](api) from external components or the [Graphical User-Interface](user-interface) of RISE.
The CRA-Controllers handles the execution of a CRA.
This includes the processing of the different **BehaviorActions** in a *State-Machine* like way (see [Main-Structures](structures)).
This means a CRA is described as a list of actions, which are executed in a predefined order.
For this each action is placed on an execution level in the ordering.
The Controller handles the execution of the different levels and processed the actions for each level.
Actions can also be started standalone or in parallel with other actions.
The execution of an action is connected with corresponding behaviors for the robot.


### Action Generator

The **Action Generator** handles the execution of each **BehaviorAction**.
During the action generation RISE checks if a queued action can be executed on the targeting robot.
This kind of modality check is used for realizing the use of one set of configurations for different robots.
A list of modalities is given for each robot in the system itself.
If an action cannot be processed because a modality is missing, the system uses an alternative execution for this modality!
A paired-list should be defined in the system for the use of different robots with one version of the application.
One special feature of the action generation is the use of the working memory.
Each parameter in an CRA can be replaced by memory keys in the configuration.
For this, the **Behavior-Controller** will load values by keys from the memory and replaces the parameters in the Messages before publishing them.
Afterwards, given parameters of an action are translated into ROS-Messages for transferring them to the robot.
This ROS-Messages are described by **HLRC-Messages** ([Repository to Actions](https://gitlab.ub.uni-bielefeld.de/rise-projects/application/tcp-connector/-/tree/include-hlrc-msgs/src/HLRC_SERVER/action)) as *ActionGoals*.

### Interactive Robot Behavior Rules
To add the functionality of interactive behavior, the *Interaction Rules* **(IR)** together with an *IR-Controller* and *Stack-Controller* were added. They get directly triggered by the *Task Scheduler*.
IRs consist out of a State Machine, with transitions dependent on incoming ROS messages, and different type of actions. One action could be the start of a Communication Robot Act. To increase the capabilities, different functions can be raised on the entry on a state, dependent on a defined if statement with memory access.
The management of these, e.g. the starting or replacing variables with values from the Memory is done by the *Interaction Controller*
To select, which Interaction Rule gets triggered next, the Stack selects the one with the highest priority that is able to take a transition with the new incoming ROS message.

This results in the possibility of creating interactive Robot Behavior.

### Environment Information Storage
The Information Memory consist of a  *Memory-Controller*, that manages the *Memory-Dict*. The Information will be provided for the Interaction Rules, Communication Robot Acts and even external *Feature Nodes* by a ROS-Service.
With this Memory, Interaction Rules can react on, by the *Feature Nodes* collected, environmental happenings. 


### User-Interface
An important part of the RISE application is the ability to monitor and control the study throw the *User-Interface*.
The RISE-GUI enables the researcher to monitor the study flow and is also able to correct and even fully manually control the study. For a full explanation of the user-interface can be found [here](user-interface.md).

#### Monitoring
In different taps the current state of IRs or CRAs is displayed. THe researcher sees which CRAs and IRs are currently running, and which CRA will be executed next after the current has finished, and even all the incoming action goals will be displayed in a list for deeper understanding.

#### Controlling
Beside the monitoring features, it is also possible to control throw the UI. The researcher is enabled to start and stop every IR and CRA and delete, change or add new values to the memory. This controlling capabilities leads to the possibility to even use [RISE as a Wizard of Oz Tool](functionalities).

### Environment and Feature-Nodes
The *Environment* represents the real world.  Here the Human interacts with the Robot.  
To be able to perceive and process some produced signals e.g. speech of the human, the *Feature-Nodes* are used. They collect information's from the *Environment* and share them with the *RISE-Core* by:
* Starting IRs and CRAs 
* Raising an interlay EventTopic 
* Assigning a Value to the WM over the ROS-ActionServer
* Getting the value of a memory key through the ROS-Memory Service
* Getting information's about the current IR State

For a full example see [How integrate RISE to own Components](functionalities.md#how-integrate-rise-to-own-components)
The goal of the Feature-Nodes is, to extend the capabilities of RISE by adding any needed input. Some examples could be natural speech detection, emotion recognition or attention tracking. The collected and data can be directly stored into the WM or only on special events be notified by raising an EventTopic.
Another use case would be e.g. a touchscreen scenario, where the beginning IR is executed when the start button is pressed, followed by raising different EventTopics dependent on the current state of the scenario. Here the Robot would always be able to explain what are the next steps when the participant clicks in this touch scenario on the help button due to the synchronization of the touchscreen scenario and the state machine of the IRs.


### Robot-Control
To be able to directly interact with the *Environment* a Robot is used. To connect any *Robot* with the *Action Generation* a *Robot-Wrapper* is placed between them. This wrapper translates the incoming HLRC Ros messages with the API of the Robot into Robot Behavior and sends back feedback. Currently, we provide a working example of a Robot-Wrapper for the NaoQi Robots **Nao** and **Pepper**. 

