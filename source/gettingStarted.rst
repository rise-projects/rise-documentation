Getting Started
===============

.. toctree::
   :maxdepth: 2

   gettingStarted/docker
   gettingStarted/installRISE
   gettingStarted/installDev
   gettingStarted/designConfigurations